# Apex Sigma: The Database Giant Discord Bot.
# Copyright (C) 2018  Lucia's Cipher
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import discord

from sigma.core.mechanics.command import SigmaCommand
from sigma.core.mechanics.payload import CommandPayload
from sigma.modules.utilities.mathematics.combinechains import combine_names


async def combinenames(_cmd: SigmaCommand, pld: CommandPayload):
    if len(pld.msg.mentions) >= 2:
        combined_name = combine_names(pld.msg.mentions)
        response = discord.Embed(color=0x3B88C3, title=f'🔤 I dub thee... {combined_name}!')
    else:
        response = discord.Embed(color=0xBE1931, title='❗ Invalid number of targets.')
    await pld.msg.channel.send(embed=response)


def cn(message: discord.Message):
    pieces = []
    total_length = sum([len(message[0]) for arg in message.content])
    usable_length = total_length // len(pld)
    needed_length = usable_length // len(pld)
    for arg in pld.msg:
        piece = pld.msg[needed_length * len(pieces):needed_length * (len(pieces) + 1)]
        pieces.append(piece)
    return ''.join(pieces)


def combineterms(_cmd: SigmaCommand, pld: CommandPayload):
    if len(pld.msg) >= 2:
        combined_term = cn(pld.msg)
        response = discord.Embed(color=0x3B88C3, title=f'🔤 I dub this amalgamation to be... {combined_term}!')
    else:
        response = discord.Embed(color=0xBE1931, title='❗ Invalid number of words.')
    await pld.msg.channel.send(embed=response)
