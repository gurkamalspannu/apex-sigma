# Apex Sigma: The Database Giant Discord Bot.
# Copyright (C) 2018  Lucia's Cipher
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import discord

from sigma.core.mechanics.command import SigmaCommand
from sigma.core.mechanics.payload import CommandPayload
from sigma.modules.minigames.other.drawcard import make_new_deck


async def newdeck(_cmd: SigmaCommand, pld: CommandPayload):
    make_new_deck(pld.msg.author.id)
    addition_title = f'✅ Your deck has been rebuilt, {pld.msg.author.display_name}.'
    response = discord.Embed(color=0x77B255, title=addition_title)
    await pld.msg.channel.send(embed=response)
